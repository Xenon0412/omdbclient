package com.greendao.generator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class GreenDaoGenerator {
    private static final int SCHEMA_VERSION = 1;
    private Schema schema = new Schema(SCHEMA_VERSION, "com.greendao.generator.omdbclient");

    private void generateSchema() throws Exception {

        Entity detailMovieInformation = schema.addEntity("DetailMovieInformation");
        detailMovieInformation.addIdProperty();
        detailMovieInformation.addStringProperty("title").notNull();
        detailMovieInformation.addStringProperty("imdbId").notNull();
        detailMovieInformation.addIntProperty("year");
        detailMovieInformation.addStringProperty("runtime");
        detailMovieInformation.addByteArrayProperty("posterBitMap");
        Property shortMovieInformationId = detailMovieInformation.addLongProperty("shortMovieInformationId").getProperty();

        Entity shortMovieInformation = schema.addEntity("ShortMovieInformation");
        shortMovieInformation.addIdProperty();
        shortMovieInformation.addStringProperty("title").notNull();
        shortMovieInformation.addStringProperty("imdbId").notNull();
        Property searchRequestId = shortMovieInformation.addLongProperty("searchRequestId").getProperty();

        detailMovieInformation.addToOne(shortMovieInformation, shortMovieInformationId);

        Entity searchRequest = schema.addEntity("SearchRequest");
        searchRequest.addIdProperty();
        searchRequest.addStringProperty("searchword").notNull();
        searchRequest.addDateProperty("created").notNull();

        searchRequest.addToMany(shortMovieInformation, searchRequestId);



        //Property detailMovieInformationId = shortMovieInformation.addLongProperty("detailMovieInformation_Id").getProperty();


        //shortMovieInformation.addToOne(detailMovieInformation, detailMovieInformationId);

        new DaoGenerator().generateAll(schema, "../app/src-greendao");
    }
    public static void main(String[] args) {
        try {
            new GreenDaoGenerator().generateSchema();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
