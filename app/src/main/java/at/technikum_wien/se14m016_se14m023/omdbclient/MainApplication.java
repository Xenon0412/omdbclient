package at.technikum_wien.se14m016_se14m023.omdbclient;

public class MainApplication extends android.app.Application {
    private DatabaseManager databaseManager;

    @Override
    public void onCreate() {
        super.onCreate();
        databaseManager = new DatabaseManager(this);
        databaseManager.initDatabaseAccess();
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }
}
