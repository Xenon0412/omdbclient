package at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects;

import android.graphics.Bitmap;

import com.google.gson.annotations.Expose;

public class DetailMovieInformation {
    public String imdbID;
    public String Title;
    public int Year;
    public String Runtime;
    public String Poster;

    @Expose
    public Bitmap PosterBitMap;
}

