package at.technikum_wien.se14m016_se14m023.omdbclient;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.greendao.generator.omdbclient.*;

import java.util.List;

import de.greenrobot.dao.query.LazyList;

public class DatabaseManager {
    private Context context;

    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private SearchRequestDao searchRequestDao;
    private ShortMovieInformationDao shortMovieInformationDao;
    private DetailMovieInformationDao detailMovieInformationDao;
    private SQLiteDatabase db;

    public DatabaseManager(Context context) {
        this.context = context;
    }

    public void initDatabaseAccess() {
        db = new DaoMaster
                .DevOpenHelper(context, "omdbclient-database", null)
                .getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        searchRequestDao = daoSession.getSearchRequestDao();
        shortMovieInformationDao = daoSession.getShortMovieInformationDao();
        detailMovieInformationDao = daoSession.getDetailMovieInformationDao();
    }

    public void closeDatabase() {
        db.close();
    }

    public void cleanDatabase() {
        searchRequestDao.deleteAll();
        shortMovieInformationDao.deleteAll();
        detailMovieInformationDao.deleteAll();
    }

    public SearchRequest getLastSearchRequest() {
        List<SearchRequest> resultList = searchRequestDao.queryBuilder().orderDesc(SearchRequestDao.Properties.Created).limit(1).list();
        if(resultList != null && resultList.size() > 0) {
            return resultList.get(0);
        }
        return null;
    }

    public LazyList<ShortMovieInformation> getShortMovieInformationListBySearchRequestId(long id) {
        return shortMovieInformationDao.queryBuilder().where(ShortMovieInformationDao.Properties.SearchRequestId.eq(id)).listLazy();
    }

    public DetailMovieInformation getDetailMovieInformationByShortMovieId(long id) {
        return detailMovieInformationDao.queryBuilder().where(DetailMovieInformationDao.Properties.ShortMovieInformationId.eq(id)).unique();
    }

    public long insertSearchRequest(SearchRequest obj) {
        return searchRequestDao.insert(obj);
    }

    public long insertShortMovieInformation(ShortMovieInformation obj) {
        return shortMovieInformationDao.insert(obj);
    }

    public long insertDetailMovieInformation(DetailMovieInformation obj) {
        return detailMovieInformationDao.insert(obj);
    }
}
