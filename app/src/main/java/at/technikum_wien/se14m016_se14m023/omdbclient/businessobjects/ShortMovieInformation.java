package at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects;

import com.google.gson.annotations.Expose;

public class ShortMovieInformation {
    public String Title;
    public String imdbID;

    @Expose
    public long databaseId;
}