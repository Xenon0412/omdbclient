package at.technikum_wien.se14m016_se14m023.omdbclient.arrayadapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.greendao.generator.omdbclient.ShortMovieInformation;

import java.util.List;

import at.technikum_wien.se14m016_se14m023.omdbclient.MovieTitleItemView;
import at.technikum_wien.se14m016_se14m023.omdbclient.MovieTitleItemView_;

public class DatabaseMovieListArrayAdapter extends ArrayAdapter<ShortMovieInformation> {

    public DatabaseMovieListArrayAdapter(Context context, List<ShortMovieInformation> objects) {
        super(context, -1, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ShortMovieInformation info = getItem(position);

        MovieTitleItemView itemView;
        if(convertView == null) {
            itemView = MovieTitleItemView_.build(getContext());
        } else {
            itemView = (MovieTitleItemView) convertView;
        }

        at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects.ShortMovieInformation guiMovieInfo =
                new at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects.ShortMovieInformation();
        guiMovieInfo.imdbID = info.getImdbId();
        guiMovieInfo.Title = info.getTitle();

        itemView.bind(guiMovieInfo);
        return itemView;
    }
}
