package at.technikum_wien.se14m016_se14m023.omdbclient.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.annotations.rest.RestService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import at.technikum_wien.se14m016_se14m023.omdbclient.DatabaseManager;
import at.technikum_wien.se14m016_se14m023.omdbclient.MainApplication;
import at.technikum_wien.se14m016_se14m023.omdbclient.R;
import at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects.DetailMovieInformation;
import at.technikum_wien.se14m016_se14m023.omdbclient.communication.OmdbClient;

@EFragment(R.layout.fragment_detail)
public class DetailFragment extends Fragment {
    private ProgressDialog mDialog;
    private DatabaseManager databaseManager;

    @ViewById(R.id.textViewMovieTitle)
    TextView titleTextView;
    @ViewById(R.id.textViewMovieImdbIDValue)
    TextView imdbIdTextView;
    @ViewById(R.id.textViewMovieReleaseYearValue)
    TextView releaseYearTextView;
    @ViewById(R.id.textViewMovieRuntimeValue)
    TextView runtimeTextView;
    @ViewById(R.id.imageViewMoviePoster)
    ImageView posterView;

    @RestService
    OmdbClient restClient;

    @FragmentArg
    boolean useCache;
    @FragmentArg
    String imdbId;
    @FragmentArg
    long shortMovieId;

    @StringRes(R.string.progress_dialog_text)
    String progressDialogText;

    public DetailFragment() {}

    @AfterViews
    void init() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseManager = ((MainApplication)getActivity().getApplicationContext()).getDatabaseManager();

        if(imdbId != null && useCache == false) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.detail_fragment_title);
            mDialog = new ProgressDialog(getActivity());
            mDialog.setMessage(progressDialogText);
            mDialog.setCancelable(false);
            mDialog.show();

            asyncSearchForDetailMovieInformation();
        } else if(useCache) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.detail_fragment_title_cache);
            searchForDetailMovieInformationInDb();
        }
    }

    @Background
    void asyncSearchForDetailMovieInformation() {
        try {
            DetailMovieInformation info = restClient.getDetailMovieInformationByImdbId(this.imdbId);
            info.PosterBitMap = downloadPoster(info.Poster);
            detailMovieSearchFinished(info);
            saveDetailInfoInDb(info);
        } catch (Exception ex) {
            detailMovieSearchFailed();
        }
    }

    @Background
    void saveDetailInfoInDb(DetailMovieInformation detailInfo) {
        com.greendao.generator.omdbclient.DetailMovieInformation detailInfoDb = new com.greendao.generator.omdbclient.DetailMovieInformation();
        detailInfoDb.setImdbId(detailInfo.imdbID);
        detailInfoDb.setTitle(detailInfo.Title);
        detailInfoDb.setRuntime(detailInfo.Runtime);
        detailInfoDb.setYear(detailInfo.Year);
        detailInfoDb.setShortMovieInformationId(this.shortMovieId);

        if(detailInfo.PosterBitMap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            detailInfo.PosterBitMap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            detailInfoDb.setPosterBitMap(byteArray);
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        databaseManager.insertDetailMovieInformation(detailInfoDb);
    }

    @Background
    void searchForDetailMovieInformationInDb() {
        com.greendao.generator.omdbclient.DetailMovieInformation detailInfo = databaseManager.getDetailMovieInformationByShortMovieId(shortMovieId);
        if(detailInfo != null) {
            DetailMovieInformation guiInfo = new DetailMovieInformation();
            guiInfo.imdbID = detailInfo.getImdbId();
            guiInfo.Title = detailInfo.getTitle();
            guiInfo.Runtime = detailInfo.getRuntime() != null ? detailInfo.getRuntime() : "";
            guiInfo.Year = detailInfo.getYear();

            if(detailInfo.getPosterBitMap() != null) {
                byte[] byteArray = detailInfo.getPosterBitMap();
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                guiInfo.PosterBitMap = bmp;
            }

            detailMovieSearchFinished(guiInfo);
        } else {
            detailMovieNotInCache();
        }
    }

    @UiThread
    void detailMovieSearchFinished(DetailMovieInformation info) {
        if (mDialog != null) mDialog.dismiss();
        if (info == null) return;

        titleTextView.setText(info.Title != null ? info.Title : "");
        imdbIdTextView.setText(info.imdbID != null ? info.imdbID : "");
        releaseYearTextView.setText(info.Year > 0 ? String.valueOf(info.Year) : "");
        runtimeTextView.setText(info.Runtime != null ? info.Runtime : "");
        if(info.PosterBitMap != null) {
            posterView.setImageBitmap(info.PosterBitMap);
        }
    }

    @UiThread
    void detailMovieSearchFailed() {
        mDialog.dismiss();
        Toast.makeText(getActivity(), R.string.detail_fragment_error_on_search, Toast.LENGTH_LONG).show();
    }

    @UiThread
    void detailMovieNotInCache() {
        Toast.makeText(getActivity(), R.string.db_no_detail_movie_information, Toast.LENGTH_LONG).show();
    }

    private Bitmap downloadPoster(String urlToPoster) {
        if(urlToPoster != null && !urlToPoster.equalsIgnoreCase("N/A")) {
            InputStream inputStream = null;
            try {
                inputStream = new URL(urlToPoster).openStream();
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
                return bitmap;
            } catch (IOException e) { }
        }
        return null;
    }
}
