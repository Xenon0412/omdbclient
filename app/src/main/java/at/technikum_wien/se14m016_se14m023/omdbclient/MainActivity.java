package at.technikum_wien.se14m016_se14m023.omdbclient;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;

import at.technikum_wien.se14m016_se14m023.omdbclient.fragments.MainFragment_;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @OptionsItem(android.R.id.home)
    void onOptionItemHomeSelected() {
        getFragmentManager().popBackStack();
    }

    @AfterViews
    void showMainFragment() {
        Fragment f = getFragmentManager().findFragmentById(R.id.fragment_placeholder);
        if (f == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.fragment_placeholder, new MainFragment_());
            transaction.commit();
        }
    }
}
