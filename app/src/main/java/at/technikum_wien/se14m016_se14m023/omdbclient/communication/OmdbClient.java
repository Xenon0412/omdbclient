package at.technikum_wien.se14m016_se14m023.omdbclient.communication;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects.DetailMovieInformation;
import at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects.Search;

@Rest(rootUrl = "http://www.omdbapi.com", converters = { GsonHttpMessageConverter.class })
public interface OmdbClient {

    @Get("/?s={title}&r=json")
    Search getMoviesByTitle(String title);

    @Get("/?i={imdbId}&plot=short&r=json")
    DetailMovieInformation getDetailMovieInformationByImdbId(String imdbId);
}
