package at.technikum_wien.se14m016_se14m023.omdbclient.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.Toast;

import com.greendao.generator.omdbclient.SearchRequest;
import com.greendao.generator.omdbclient.ShortMovieInformation;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import at.technikum_wien.se14m016_se14m023.omdbclient.DatabaseManager;
import at.technikum_wien.se14m016_se14m023.omdbclient.MainApplication;
import at.technikum_wien.se14m016_se14m023.omdbclient.arrayadapters.DatabaseMovieListArrayAdapter;
import at.technikum_wien.se14m016_se14m023.omdbclient.arrayadapters.MovieListArrayAdapter;
import at.technikum_wien.se14m016_se14m023.omdbclient.R;
import at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects.Search;
import de.greenrobot.dao.query.LazyList;

@EFragment(R.layout.fragment_cached_movie_titles)
public class CachedMovieTitlesFragment extends Fragment {
    private DatabaseMovieListArrayAdapter arrayAdapter;
    private DatabaseManager databaseManager;
    private LazyList lazyList;

    @ViewById(R.id.listViewCachedMovies)
    ListView listView;

    @FragmentArg
    long dbSearchRequestId;

    public CachedMovieTitlesFragment() {}

    @AfterViews
    void init() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.cached_movie_titles_frsgment_title);

        databaseManager = ((MainApplication)getActivity().getApplicationContext()).getDatabaseManager();
        lazyList = databaseManager.getShortMovieInformationListBySearchRequestId(this.dbSearchRequestId);

        arrayAdapter = new DatabaseMovieListArrayAdapter(getActivity(), lazyList);
        listView.setAdapter(arrayAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(lazyList != null) { lazyList.close(); }
    }

    @ItemClick(R.id.listViewCachedMovies)
    void listViewItemClicked(int position) {
        ShortMovieInformation info = arrayAdapter.getItem(position);
        performSwitchToDetailFragment(info.getId());
    }

    private void performSwitchToDetailFragment(long databaseId) {
        Fragment detailFragment = DetailFragment_.builder().shortMovieId(databaseId).useCache(true).build();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_placeholder, detailFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
