package at.technikum_wien.se14m016_se14m023.omdbclient.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.greendao.generator.omdbclient.SearchRequest;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.annotations.rest.RestService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import at.technikum_wien.se14m016_se14m023.omdbclient.DatabaseManager;
import at.technikum_wien.se14m016_se14m023.omdbclient.MainApplication;
import at.technikum_wien.se14m016_se14m023.omdbclient.arrayadapters.MovieListArrayAdapter;
import at.technikum_wien.se14m016_se14m023.omdbclient.R;
import at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects.Search;
import at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects.ShortMovieInformation;
import at.technikum_wien.se14m016_se14m023.omdbclient.communication.OmdbClient;

@EFragment(R.layout.fragment_main)
public class MainFragment extends Fragment {
    private MovieListArrayAdapter arrayAdapter;
    private ProgressDialog mDialog;
    private DatabaseManager databaseManager;

    @ViewById(R.id.listViewMovies)
    ListView listView;
    @ViewById(R.id.editTextMovieTitle)
    EditText editTextMovieTitle;

    @RestService
    OmdbClient restClient;

    @StringRes(R.string.progress_dialog_text)
    String progressDialogText;

    public MainFragment() {}

    @AfterViews
    void init() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.main_fragment_title);

        if(arrayAdapter == null) {
            arrayAdapter = new MovieListArrayAdapter(getActivity(), new ArrayList<ShortMovieInformation>());
        }
        listView.setAdapter(arrayAdapter);
        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage(progressDialogText);
        mDialog.setCancelable(false);

        databaseManager = ((MainApplication)getActivity().getApplicationContext()).getDatabaseManager();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @ItemClick(R.id.listViewMovies)
    void listViewItemClicked(int position) {
        ShortMovieInformation info = arrayAdapter.getItem(position);
        performSwitchToDetailFragment(info.imdbID, info.databaseId);
    }

    @Click(R.id.buttonSearch)
    void searchButtonClicked() {
        String trimmedTitle = editTextMovieTitle.getText().toString().trim();
        if(trimmedTitle.equals("")) { return; }
        dismissKeyboard();
        mDialog.show();
        asyncSearchForMovieTitles(trimmedTitle);
    }

    @Click(R.id.buttonShowLastRequest)
    void showLastMovieRequest() {
        SearchRequest searchRequest = databaseManager.getLastSearchRequest();
        if(searchRequest != null) {
            performSwitchToCachedMovieTitlesFragment(searchRequest.getId());
        } else {
            Toast.makeText(getActivity(), R.string.db_no_search_requests, Toast.LENGTH_LONG).show();
        }
    }

    @Background
    void asyncSearchForMovieTitles(String movieTitle) {
        try {
            Search search = restClient.getMoviesByTitle(movieTitle);
            if(search == null) {
                movieTitleSearchFailed();
            } else {
                movieTitleSearchFinished(search.movieList);
                saveSearchInDb(search, movieTitle);
            }
        } catch (Exception ex) {
            movieTitleSearchFailed();
        }
    }

    @UiThread
    void movieTitleSearchFinished(List<ShortMovieInformation> resultList) {
        if(resultList == null) { return; }
        arrayAdapter.clear();
        arrayAdapter.addAll(resultList);
        arrayAdapter.notifyDataSetChanged();
        mDialog.dismiss();
    }

    @UiThread
    void movieTitleSearchFailed() {
        mDialog.dismiss();
        Toast.makeText(getActivity(), R.string.main_fragment_error_on_search, Toast.LENGTH_LONG).show();
    }

    @Background
    void saveSearchInDb(Search search, String searchedMovieTitle) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setCreated(new Date());
        searchRequest.setSearchword(searchedMovieTitle);
        long searchRequestDbId = databaseManager.insertSearchRequest(searchRequest);

        for(ShortMovieInformation shortInfo : search.movieList) {
            com.greendao.generator.omdbclient.ShortMovieInformation dbInfo = new com.greendao.generator.omdbclient.ShortMovieInformation();
            dbInfo.setSearchRequestId(searchRequestDbId);
            dbInfo.setImdbId(shortInfo.imdbID);
            dbInfo.setTitle(shortInfo.Title);
            long dbInfoId = databaseManager.insertShortMovieInformation(dbInfo);
            shortInfo.databaseId = dbInfoId;
        }
    }

    private void dismissKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void performSwitchToDetailFragment(String imdbId, long shortMovieDbId) {
        Fragment detailFragment = DetailFragment_.builder().imdbId(imdbId).shortMovieId(shortMovieDbId).build();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_placeholder, detailFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void performSwitchToCachedMovieTitlesFragment(long searchRequestId) {
        Fragment cachedMovieTitlesFragment = CachedMovieTitlesFragment_.builder().dbSearchRequestId(searchRequestId).build();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_placeholder, cachedMovieTitlesFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
