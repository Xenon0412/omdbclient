package at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Search {
    @SerializedName("Search")
    public List<ShortMovieInformation> movieList;
}
