package at.technikum_wien.se14m016_se14m023.omdbclient;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import at.technikum_wien.se14m016_se14m023.omdbclient.businessobjects.ShortMovieInformation;

@EViewGroup(android.R.layout.simple_list_item_1)
public class MovieTitleItemView extends LinearLayout {

    @ViewById(android.R.id.text1)
    TextView textView;

    public MovieTitleItemView(Context context) {
        super(context);
    }

    public void bind(ShortMovieInformation info) {
        textView.setText(info.Title);
    }
}
