package com.greendao.generator.omdbclient;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

import com.greendao.generator.omdbclient.DetailMovieInformation;
import com.greendao.generator.omdbclient.ShortMovieInformation;
import com.greendao.generator.omdbclient.SearchRequest;

import com.greendao.generator.omdbclient.DetailMovieInformationDao;
import com.greendao.generator.omdbclient.ShortMovieInformationDao;
import com.greendao.generator.omdbclient.SearchRequestDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig detailMovieInformationDaoConfig;
    private final DaoConfig shortMovieInformationDaoConfig;
    private final DaoConfig searchRequestDaoConfig;

    private final DetailMovieInformationDao detailMovieInformationDao;
    private final ShortMovieInformationDao shortMovieInformationDao;
    private final SearchRequestDao searchRequestDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        detailMovieInformationDaoConfig = daoConfigMap.get(DetailMovieInformationDao.class).clone();
        detailMovieInformationDaoConfig.initIdentityScope(type);

        shortMovieInformationDaoConfig = daoConfigMap.get(ShortMovieInformationDao.class).clone();
        shortMovieInformationDaoConfig.initIdentityScope(type);

        searchRequestDaoConfig = daoConfigMap.get(SearchRequestDao.class).clone();
        searchRequestDaoConfig.initIdentityScope(type);

        detailMovieInformationDao = new DetailMovieInformationDao(detailMovieInformationDaoConfig, this);
        shortMovieInformationDao = new ShortMovieInformationDao(shortMovieInformationDaoConfig, this);
        searchRequestDao = new SearchRequestDao(searchRequestDaoConfig, this);

        registerDao(DetailMovieInformation.class, detailMovieInformationDao);
        registerDao(ShortMovieInformation.class, shortMovieInformationDao);
        registerDao(SearchRequest.class, searchRequestDao);
    }
    
    public void clear() {
        detailMovieInformationDaoConfig.getIdentityScope().clear();
        shortMovieInformationDaoConfig.getIdentityScope().clear();
        searchRequestDaoConfig.getIdentityScope().clear();
    }

    public DetailMovieInformationDao getDetailMovieInformationDao() {
        return detailMovieInformationDao;
    }

    public ShortMovieInformationDao getShortMovieInformationDao() {
        return shortMovieInformationDao;
    }

    public SearchRequestDao getSearchRequestDao() {
        return searchRequestDao;
    }

}
